﻿namespace SmartDeviceProject1.interfaz
{
    partial class NoAcces
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NoAcces));
            this.panelSave = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelLoadPhoto = new System.Windows.Forms.Panel();
            this.lblUploadPhoto = new System.Windows.Forms.Label();
            this.pTakePhoto = new System.Windows.Forms.PictureBox();
            this.panelDireccion = new System.Windows.Forms.Panel();
            this.tAdrres = new System.Windows.Forms.TextBox();
            this.panelCodService = new System.Windows.Forms.Panel();
            this.tCodeService = new System.Windows.Forms.TextBox();
            this.panelDateHour = new System.Windows.Forms.Panel();
            this.tDateHour = new System.Windows.Forms.TextBox();
            this.panelCoordenada = new System.Windows.Forms.Panel();
            this.tCoordenate = new System.Windows.Forms.TextBox();
            this.panelMotive = new System.Windows.Forms.Panel();
            this.cMotivo = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.back = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panelSave.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panelLoadPhoto.SuspendLayout();
            this.panelDireccion.SuspendLayout();
            this.panelCodService.SuspendLayout();
            this.panelDateHour.SuspendLayout();
            this.panelCoordenada.SuspendLayout();
            this.panelMotive.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSave
            // 
            this.panelSave.BackColor = System.Drawing.Color.Gray;
            this.panelSave.Controls.Add(this.label5);
            this.panelSave.Location = new System.Drawing.Point(0, 264);
            this.panelSave.Name = "panelSave";
            this.panelSave.Size = new System.Drawing.Size(120, 20);
            this.panelSave.Click += new System.EventHandler(this.panelSave_Click);
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 20);
            this.label5.Text = "GUARDAR .";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(111)))), ((int)(((byte)(222)))));
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(0, 238);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(120, 20);
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 20);
            this.label4.Text = "MOTIVO .";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(88)))), ((int)(((byte)(203)))));
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(0, 212);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(120, 20);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.Text = "COORDENADAS .";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(0, 186);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(120, 20);
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.Text = "FECHA Y HORA .";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.panel5.Controls.Add(this.label1);
            this.panel5.Location = new System.Drawing.Point(0, 160);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(120, 20);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.Text = "CODIGO SERVICIO .";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelLoadPhoto
            // 
            this.panelLoadPhoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.panelLoadPhoto.Controls.Add(this.lblUploadPhoto);
            this.panelLoadPhoto.Controls.Add(this.pTakePhoto);
            this.panelLoadPhoto.Location = new System.Drawing.Point(0, 81);
            this.panelLoadPhoto.Name = "panelLoadPhoto";
            this.panelLoadPhoto.Size = new System.Drawing.Size(120, 73);
            this.panelLoadPhoto.Click += new System.EventHandler(this.panelLoadPhoto_Click);
            // 
            // lblUploadPhoto
            // 
            this.lblUploadPhoto.ForeColor = System.Drawing.Color.White;
            this.lblUploadPhoto.Location = new System.Drawing.Point(3, 36);
            this.lblUploadPhoto.Name = "lblUploadPhoto";
            this.lblUploadPhoto.Size = new System.Drawing.Size(120, 20);
            this.lblUploadPhoto.Text = "SUBIR FOTO .";
            this.lblUploadPhoto.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pTakePhoto
            // 
            this.pTakePhoto.Location = new System.Drawing.Point(17, 0);
            this.pTakePhoto.Name = "pTakePhoto";
            this.pTakePhoto.Size = new System.Drawing.Size(73, 73);
            this.pTakePhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pTakePhoto.Visible = false;
            // 
            // panelDireccion
            // 
            this.panelDireccion.BackColor = System.Drawing.Color.Transparent;
            this.panelDireccion.Controls.Add(this.tAdrres);
            this.panelDireccion.Location = new System.Drawing.Point(126, 105);
            this.panelDireccion.Name = "panelDireccion";
            this.panelDireccion.Size = new System.Drawing.Size(120, 20);
            // 
            // tAdrres
            // 
            this.tAdrres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tAdrres.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tAdrres.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.tAdrres.Location = new System.Drawing.Point(0, 0);
            this.tAdrres.Name = "tAdrres";
            this.tAdrres.Size = new System.Drawing.Size(120, 21);
            this.tAdrres.TabIndex = 0;
            this.tAdrres.Text = "San Martin Nº4532";
            // 
            // panelCodService
            // 
            this.panelCodService.BackColor = System.Drawing.Color.Transparent;
            this.panelCodService.Controls.Add(this.tCodeService);
            this.panelCodService.Location = new System.Drawing.Point(126, 160);
            this.panelCodService.Name = "panelCodService";
            this.panelCodService.Size = new System.Drawing.Size(120, 20);
            // 
            // tCodeService
            // 
            this.tCodeService.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tCodeService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tCodeService.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.tCodeService.Location = new System.Drawing.Point(0, 0);
            this.tCodeService.Name = "tCodeService";
            this.tCodeService.Size = new System.Drawing.Size(120, 21);
            this.tCodeService.TabIndex = 1;
            this.tCodeService.TextChanged += new System.EventHandler(this.tCodeService_TextChanged);
            this.tCodeService.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tCodeService_KeyPress);
            // 
            // panelDateHour
            // 
            this.panelDateHour.BackColor = System.Drawing.Color.Transparent;
            this.panelDateHour.Controls.Add(this.tDateHour);
            this.panelDateHour.Location = new System.Drawing.Point(126, 186);
            this.panelDateHour.Name = "panelDateHour";
            this.panelDateHour.Size = new System.Drawing.Size(120, 20);
            // 
            // tDateHour
            // 
            this.tDateHour.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tDateHour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tDateHour.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.tDateHour.Location = new System.Drawing.Point(0, 0);
            this.tDateHour.Name = "tDateHour";
            this.tDateHour.Size = new System.Drawing.Size(120, 21);
            this.tDateHour.TabIndex = 1;
            // 
            // panelCoordenada
            // 
            this.panelCoordenada.BackColor = System.Drawing.Color.Transparent;
            this.panelCoordenada.Controls.Add(this.tCoordenate);
            this.panelCoordenada.Location = new System.Drawing.Point(126, 212);
            this.panelCoordenada.Name = "panelCoordenada";
            this.panelCoordenada.Size = new System.Drawing.Size(120, 20);
            // 
            // tCoordenate
            // 
            this.tCoordenate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tCoordenate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tCoordenate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(88)))), ((int)(((byte)(203)))));
            this.tCoordenate.Location = new System.Drawing.Point(0, 0);
            this.tCoordenate.Multiline = true;
            this.tCoordenate.Name = "tCoordenate";
            this.tCoordenate.Size = new System.Drawing.Size(120, 20);
            this.tCoordenate.TabIndex = 1;
            // 
            // panelMotive
            // 
            this.panelMotive.BackColor = System.Drawing.Color.Transparent;
            this.panelMotive.Controls.Add(this.cMotivo);
            this.panelMotive.Location = new System.Drawing.Point(126, 238);
            this.panelMotive.Name = "panelMotive";
            this.panelMotive.Size = new System.Drawing.Size(115, 20);
            // 
            // cMotivo
            // 
            this.cMotivo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cMotivo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cMotivo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(111)))), ((int)(((byte)(222)))));
            this.cMotivo.Items.Add("Motivo 1");
            this.cMotivo.Items.Add("Motivo 2");
            this.cMotivo.Items.Add("Motivo 3");
            this.cMotivo.Items.Add("Motivo 4");
            this.cMotivo.Items.Add("Motivo 5");
            this.cMotivo.Items.Add("Motivo 6");
            this.cMotivo.Items.Add("Motivo 7");
            this.cMotivo.Items.Add("Motivo 8");
            this.cMotivo.Items.Add("Motivo 9");
            this.cMotivo.Location = new System.Drawing.Point(0, 0);
            this.cMotivo.Name = "cMotivo";
            this.cMotivo.Size = new System.Drawing.Size(115, 22);
            this.cMotivo.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(81, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(84, 54);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(0, 25);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(40, 40);
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // listBox1
            // 
            this.listBox1.Location = new System.Drawing.Point(126, 180);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(100, 100);
            this.listBox1.TabIndex = 13;
            this.listBox1.Visible = false;
            this.listBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_SelectedValueChanged);
            // 
            // NoAcces
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.back);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panelDireccion);
            this.Controls.Add(this.panelCodService);
            this.Controls.Add(this.panelDateHour);
            this.Controls.Add(this.panelCoordenada);
            this.Controls.Add(this.panelMotive);
            this.Controls.Add(this.panelLoadPhoto);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelSave);
            this.Name = "NoAcces";
            this.Size = new System.Drawing.Size(240, 280);
            this.panelSave.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panelLoadPhoto.ResumeLayout(false);
            this.panelDireccion.ResumeLayout(false);
            this.panelCodService.ResumeLayout(false);
            this.panelDateHour.ResumeLayout(false);
            this.panelCoordenada.ResumeLayout(false);
            this.panelMotive.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSave;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panelLoadPhoto;
        private System.Windows.Forms.Panel panelDireccion;
        private System.Windows.Forms.TextBox tAdrres;
        private System.Windows.Forms.Panel panelCodService;
        private System.Windows.Forms.Panel panelDateHour;
        private System.Windows.Forms.Panel panelCoordenada;
        private System.Windows.Forms.Panel panelMotive;
        private System.Windows.Forms.TextBox tCodeService;
        private System.Windows.Forms.TextBox tDateHour;
        private System.Windows.Forms.TextBox tCoordenate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cMotivo;
        private System.Windows.Forms.PictureBox pTakePhoto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblUploadPhoto;
        private System.Windows.Forms.Panel back;
        private System.Windows.Forms.ListBox listBox1;
    }
}
